package com.tsc.apogasiy.tm;

import com.tsc.apogasiy.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        parseArgs(args);
        process();
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            parseArg(command);
            System.out.println();
        }
    }

    private static void displayHelp() {
        System.out.println(TerminalConst.CMD_VERSION + " - Display program version.");
        System.out.println(TerminalConst.CMD_ABOUT + " - Display developer info.");
        System.out.println(TerminalConst.CMD_HELP + " - Display list of terminal commands.");
        System.out.println(TerminalConst.CMD_EXIT + " - Close application.");
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayAbout() {
        System.out.println("Alexey Pogasiy");
        System.out.println("apogasiy@tsconsulting.com");
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void displayError() {
        System.out.println("Unknown command! Type 'help' for supportable command!");
    }

    private static void parseArg(final String arg) {
        switch (arg) {
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                displayError();
                break;
        }
    }

    private static void parseArgs(final String[] args) {
        if (args == null || args.length == 0)
            return;
        for (String param : args)
            parseArg(param);
    }

}
